#!/usr/bin/env python3

from json import load
from sys import stdin


CHANNEL_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
  <title><![CDATA[{title}]]></title>
  <description><![CDATA[{description}]]></description>
  <link>{link}</link>
  <language>{language}</language>
  <copyright><![CDATA[{copyright}]]></copyright>
  <category>{category}</category>
  <generator>https://codeberg.org/karam/genrssfeed</generator>
  <docs>https://www.rssboard.org/rss-specification</docs>
  <ttl>{ttl}</ttl>
  <image>
    <url>{favicon}</url>
    <title><![CDATA[{title}]]></title>
    <link>{link}</link>
  </image>
  <atom:link href="{url}" rel="self" type="application/rss+xml"/>

{items}

</channel>
</rss>
""".strip("\n")

ITEM_TEMPLATE = """
  <item>
    <title><![CDATA[{title}]]></title>
    <description><![CDATA[{description}]]></description>
    <link>{link}</link>
    <author>{email} (<![CDATA[{author}]]>)</author>
    <category>{category}</category>
    <guid isPermaLink="{ispermalink}">{guid}</guid>
  </item>
""".strip("\n")


if __name__ == "__main__":

    inp = load(stdin)

    print(CHANNEL_TEMPLATE.format(
            **inp["channel"],
            items = "\n".join([
                ITEM_TEMPLATE.format(**item, **inp["global_item"])
                for item in inp["items"]])))
