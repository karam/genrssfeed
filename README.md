## gen_rss_feed.py

A simple Python script that I use to generate RSS feeds for my own blog.  You
may want to edit the `CHANNEL_TEMPALTE` and `ITEM_TEMPLATE` it to fit your
needs.

### Usage

Make your own `feed.json` from `feed.json.template`, then:

```
./gen_rss_feed.py < feed.json > feed.xml
```

See the [example/](./example/) directory for an already compiled example.
